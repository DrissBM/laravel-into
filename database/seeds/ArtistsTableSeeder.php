<?php

use Illuminate\Database\Seeder;

class ArtistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('artists')->insert([[
            'name' => 'Praz',
            'firstname' => 'Vincent',
            'birthdate' => 1959,
        ],
        [
            'name' => 'Ben Milune',
            'firstname' => 'Driss',
            'birthdate' => 2006,
        ],
        [
            'name' => 'Da costa croisière',
            'firstname' => 'Lili',
            'birthdate' => 1999,
        ],
        [
            'name' => 'Karouwie',
            'firstname' => 'Rania',
            'birthdate' => 1973,
        ],
        [
            'name' => 'Lijeardo',
            'firstname' => 'Rémy',
            'birthdate' => 1989,
        ],
        [
            'name' => 'Gairaierot',
            'firstname' => 'André',
            'birthdate' => 1993,
        ],
        [
            'name' => 'Tourte',
            'firstname' => 'Maka',
            'birthdate' => 1901,
        ],
        [
            'name' => 'Tab &',
            'firstname' => 'Hasinah',
            'birthdate' => 1902,
        ],
        [
            'name' => 'Pense',
            'firstname' => 'Roxy',
            'birthdate' => 1969,
        ],
        [
            'name' => 'Madara',
            'firstname' => 'Alioune',
            'birthdate' => 1923,
        ]]);
    }
}
