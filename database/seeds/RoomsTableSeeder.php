<?php

use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('rooms')->insert([[
            'name' => 'Salle 1',
            'capacity' => 165,
            'cinema_id' => 1,
        ],
        [
            'name' => 'Salle 2',
            'capacity' => 300,
            'cinema_id' => 2,
        ],
        [
            'name' => 'Salle 3',
            'capacity' => 250,
            'cinema_id' => 3,
        ],
        [
            'name' => 'Salle 4',
            'capacity' => 200,
            'cinema_id' => 4,
        ],
        [
            'name' => 'Salle 5',
            'capacity' => 160,
            'cinema_id' => 5,
        ],
        [
            'name' => 'Salle 6',
            'capacity' => 220,
            'cinema_id' => 6,
        ],
        [
            'name' => 'Salle 7',
            'capacity' => 270,
            'cinema_id' => 7,
        ],
        [
            'name' => 'Salle 8',
            'capacity' => 280,
            'cinema_id' => 8,
        ],
        [
            'name' => 'Salle 9',
            'capacity' => 200,
            'cinema_id' => 9,
        ],
        [
            'name' => 'Salle 10',
            'capacity' => 190,
            'cinema_id' => 10,
        ],
        [
            'name' => 'Salle 12',
            'capacity' => 110,
            'cinema_id' => 5,
        ]]);
    }
}
