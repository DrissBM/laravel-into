<?php

use Illuminate\Database\Seeder;

class CinemaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('cinemas')->insert([[
            'name' => 'Arlequin',
            'street' => "Av. de la gare 1",
            'npa' => 1950,
            'city' => "Sion",
            'country' => "Suisse",
        ], [
            'name' => 'Lux',
            'street' => "Av. de la gare 2",
            'npa' => 1950,
            'city' => "Sion",
            'country' => "Suisse",
        ], [
            'name' => 'Capitole',
            'street' => "Avenue du Théâtre 6",
            'npa' => 1005,
            'city' => "Lausanne",
            'country' => "Suisse",
        ], [
            'name' => 'Nord-Sud',
            'street' => "Rue de la Servette 78",
            'npa' => 1202,
            'city' => "Genève",
            'country' => "Suisse",
        ], [
            'name' => 'Cine17',
            'street' => "Rue de la Corraterie 17",
            'npa' => 1202,
            'city' => "Genève",
            'country' => "Suisse",
        ], [
            'name' => 'Spoutnik',
            'street' => "Place des Volontaires 4",
            'npa' => 1202,
            'city' => "Genève",
            'country' => "Suisse",
        ], [
            'name' => 'CinemaSplendid',
            'street' => "Place De-Grenus 3",
            'npa' => 1202,
            'city' => "Genève",
            'country' => "Suisse",
        ], [
            'name' => 'Cinéma CDD',
            'street' => "Sentier des Saules 3",
            'npa' => 1202,
            'city' => "Genève",
            'country' => "Suisse",
        ], [
            'name' => 'Le Poche Genève',
            'street' => "Rue du Cheval-Blanc 4",
            'npa' => 1202,
            'city' => "Genève",
            'country' => "Suisse",
        ], [
            'name' => 'Cinéma Bio',
            'street' => "Rue Saint-Joseph 47",
            'npa' => 1202,
            'city' => "Genève",
            'country' => "Suisse",
        ]]);
    }
}
