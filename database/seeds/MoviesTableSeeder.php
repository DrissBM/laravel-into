<?php

use App\Models\Movie;
use Illuminate\Database\Seeder;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('movies')->insert([[
            'title' => 'Vincent et le retour du pharaon',
            'year' => 1939,
            'artist_id' => 1
        ],
        [
            'title' => 'Vincent en Namibi',
            'year' => 1946,
            'artist_id' => 2
        ],
        [
            'title' => 'Sueurs froides',
            'year' => 1966,
            'artist_id' => 5
        ],
        [
            'title' => 'Citizen Kane',
            'year' => 1936,
            'artist_id' => 7
        ],
        [
            'title' => 'Voyage à Tokyo',
            'year' => 1978,
            'artist_id' => 8
        ],
        [
            'title' => 'La Règle du jeu',
            'year' => 1953,
            'artist_id' => 3
        ],
        [
            'title' => 'L\'Aurore',
            'year' => 1922,
            'artist_id' => 4
        ],
        [
            'title' => '2001, l\'odyssée de l\'espace',
            'year' => 1951,
            'artist_id' => 6
        ],
        [
            'title' => 'La Prisonnière du désert',
            'year' => 1988,
            'artist_id' => 9
        ],
        [
            'title' => 'L\'Homme à la caméra',
            'year' => 1902,
            'artist_id' => 10
        ]
        ]);
        Movie::all()->each(function ($user){
            $show_time = '2020-04-'. rand(1,30) . 'T12:00';
            $user->isPlayed()->attach(rand(1, 10), ['show_time' => $show_time]);
            $user->actors()->attach(rand(1, 10));
            $user->actors()->attach(rand(1, 10));
        });
    }
}
