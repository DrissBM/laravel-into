<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movie extends Model
{
    //
    use SoftDeletes;
    protected $fillable = [
        'title', 'year', 'artist_id'
    ];
    public function director() {
        return $this->belongsTo(Artist::class, 'artist_id');
    }
    public function actors() {
        return $this->belongsToMany(Artist::class);
    }

    public function isPlayed() {
        return $this->belongsToMany(Room::class)->withPivot('show_time');
    }
}
