<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cinema extends Model
{
    //
    use SoftDeletes;
    protected $fillable = [
        'name', 'street', 'npa', 'city', 'country'
    ];
    public function has_owned() {
        return $this->hasMany('App\Models\Room');
    }
}
