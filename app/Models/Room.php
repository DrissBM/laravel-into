<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{
    //
    use SoftDeletes;
    protected $fillable = [
        'name', 'capacity', 'cinema_id'
    ];
    public function owned() {
        return $this->belongsTo(Cinema::class, 'cinema_id');
    }

    public function play() {
        return $this->belongsToMany(Movie::class)->withPivot('show_time');
    }
}
