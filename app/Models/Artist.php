<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Artist extends Model
{
    //
    use SoftDeletes;
    protected $fillable = [
        'name', 'firstname', 'birthdate'
    ];
    public function has_directed() {
        return $this->hasMany('App\Models\Movie');
    }
    public function has_played() {
        return $this->belongsToMany('App\Models\Movie')->withPivot('role_name');
    }
}
