<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Http\Requests\ArtistRequest;
use Exception;
use Illuminate\Http\Response;
use Image;

use Illuminate\Http\Request;

class ArtistController extends Controller
{
    public function __construct()
    {
        $this->middleware('ajax')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        return view('artists.index', ['artists' => Artist::paginate(6)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('artists.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ArtistRequest $request
     * @return Response
     */
    public function store(ArtistRequest $request)
    {
        //
        $artist = Artist::create($request->all());
        $poster = $request->file('img');
        if ($poster) {
            $filename = 'face_' . $artist->id . '.' . $poster->getClientOriginalExtension();
            Image::make($poster)->fit(180, 240)
                ->save(public_path('/uploads/face/' . $filename));

        }
        return redirect()->route('artist.create')
            ->with('ok', __('Artist has been saved'));
    }

    /**
     * Display the specified resource.
     *
     * @param Artist $artist
     * @return Response
     */
    public function show(Artist $artist)
    {
        return view('artists.show', ['artist' => $artist]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Artist $artist
     * @return Response
     */
    public function edit(Artist $artist)
    {
        //
        return view('artists.edit', ['artist' => $artist]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ArtistRequest $request
     * @param Artist $artist
     * @return Response
     */
    public function update(ArtistRequest $request, Artist $artist)
    {
        //
        $artist->update($request->all());

        $poster = $request->file('img');
        if ($poster) {
            $filename = 'face_' . $artist->id . '.' . $poster->getClientOriginalExtension();
            Image::make($poster)->fit(180, 240)
                ->save(public_path('/uploads/face/' . $filename));

        }

        return redirect()->route('artist.index')
            ->with('ok', __('Artist has been updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Artist $artist
     * @return Response
     * @throws Exception
     */
    public function destroy(Artist $artist)
    {
        //
        $artist->has_directed()->delete();
        $artist->has_played()->detach();
        $artist->delete();
        return response()->json();
    }
}
