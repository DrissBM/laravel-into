<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\Movie;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        return view('welcome', ['artists' => Artist::orderBy('id', 'desc')->limit(10)->get(), 'moviesList' => Movie::orderBy('id', 'desc')->limit(10)->get(), 'seances' => DB::table('movie_room')->join('movies', 'movie_room.movie_id', '=', 'movies.id')->join('rooms', 'movie_room.room_id', '=', 'rooms.id')->distinct()->limit(10)->get()]);
    }
}
