<?php

namespace App\Http\Controllers;

use App\Models\Cinema;
use App\Models\Room;
use App\Http\Requests\RoomRequest;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RoomController extends Controller
{
    public function __construct()
    {
        $this->middleware('ajax')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        return view('rooms.index', ['rooms' => Room::paginate(6)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('rooms.create', ['cinemas' => Cinema::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RoomRequest $request
     * @return Response
     */
    public function store(RoomRequest $request)
    {
        //
        Room::create($request->all());
        return redirect()->route('room.create')
            ->with('ok', __('Room has been saved'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Room $room
     * @return Response
     */
    public function edit(Room $room)
    {
        //
        return view('rooms.edit', ['room' => $room, 'cinemas' => Cinema::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RoomRequest $request
     * @param Room $room
     * @return Response
     */
    public function update(RoomRequest $request, Room $room)
    {
        //
        $room->update($request->all());
        return redirect()->route('room.index')
            ->with('ok', __('Room has been updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Room $room
     * @return Response
     * @throws Exception
     */
    public function destroy(Room $room)
    {
        //
        $room->play()->detach();
        $room->delete();
        return response()->json();
    }
}
