<?php

namespace App\Http\Controllers;

use App\Models\Cinema;
use App\Http\Requests\CinemaRequest;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CinemaController extends Controller
{
    public function __construct() {
        $this->middleware('ajax')->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        return view('cinemas.index', [ 'cinemas' => Cinema::paginate(6) ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('cinemas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CinemaRequest $request
     * @return Response
     */
    public function store(CinemaRequest $request)
    {
        //
        Cinema::create($request->all());
        return redirect()->route('cinema.create')
                        ->with('ok', __('Cinema has been saved'));
    }

    /**
     * Display the specified resource.
     *
     * @param  Cinema  $cinema
     * @return Response
     */
    public function show(Cinema $cinema)
    {
        return view('cinemas.show', ['cinema' => $cinema]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Cinema $cinema
     * @return Response
     */
    public function edit(Cinema $cinema)
    {
        //
        return view('cinemas.edit', ['cinema' => $cinema]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CinemaRequest $request
     * @param Cinema $cinema
     * @return Response
     */
    public function update(CinemaRequest $request, Cinema $cinema)
    {
        //
        $cinema->update( $request->all() );
        return redirect()->route('cinema.index')
                        ->with( 'ok', __('Cinema has been updated') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Cinema $cinema
     * @return Response
     * @throws Exception
     */
    public function destroy(Cinema $cinema)
    {
        //
        $cinema->has_owned()->delete();
        $cinema->delete();
        return response()->json();
    }
}
