<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use App\Models\Artist;
use App\Http\Requests\MovieRequest;
use App\Models\Room;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Response;
use Image;

use Illuminate\Http\Request;

class MovieController extends Controller
{
    public function __construct()
    {
        $this->middleware('ajax')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        return view('movies.index', ['movies' => Movie::paginate(6)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('movies.create', ['artists' => Artist::all(), 'rooms' => Room::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param MovieRequest $request
     * @return Response
     */
    public function store(MovieRequest $request)
    {
        //
        $movie = Movie::create($request->all());
        $poster = $request->file('img');
        if ($poster) {
            $filename = 'poster_' . $movie->id . '.' . $poster->getClientOriginalExtension();
            Image::make($poster)->fit(180, 240)
                ->save(public_path('/uploads/posters/' . $filename));
        }
        $actor1 = $request->input('actor_id1');
        $actor2 = $request->input('actor_id2');
        $room_id = $request->input('room_id');
        $show_time = $request->input('show_time');
        $movie->isPlayed()->attach($room_id, ['show_time' => $show_time]);
        $movie->actors()->attach($actor1);
        $movie->actors()->attach($actor2);
        return redirect()->route('movie.create')
            ->with('ok', __('Movie has been saved'));
    }

    /**
     * Display the specified resource.
     *
     * @param Movie $movie
     * @return Response
     */
    public function show(Movie $movie)
    {
        return view('movies.show', ['movie' => $movie]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Movie $movie
     * @return Response
     */
    public function edit(Movie $movie)
    {
        // Formatting date to retrieve the old value
        $date = Carbon::parse($movie->isPlayed()->first()->pivot->show_time)->format('Y-m-d');
        $dateWithT = $date . 'T';
        $date_sec = Carbon::parse($movie->isPlayed()->first()->pivot->show_time)->format('h:i');
        $date = $dateWithT . $date_sec;

        return view('movies.edit', ['movie' => $movie, 'artists' => Artist::all(), 'rooms' => Room::all(), 'date' => $date]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param MovieRequest $request
     * @param Movie $movie
     * @return Response
     */
    public function update(MovieRequest $request, Movie $movie)
    {
        //
        $movie->update($request->all());
        $poster = $request->file('img');
        if ($poster) {
            $filename = 'poster_' . $movie->id . '.' . $poster->getClientOriginalExtension();
            Image::make($poster)->fit(180, 240)
                ->save(public_path('/uploads/posters/' . $filename));
        }
        $actor1 = $request->input('actor_id1');
        $actor2 = $request->input('actor_id2');
        $room_id = $request->input('room_id');
        $show_time = $request->input('show_time');

        // detach + attach because sync is having a weird behavior
        $movie->isPlayed()->detach();
        $movie->actors()->detach();

        $movie->isPlayed()->attach($room_id, ['show_time' => $show_time]);
        $movie->actors()->attach($actor1);
        $movie->actors()->attach($actor2);


        return redirect()->route('movie.index')
            ->with('ok', __('Movie has been updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Movie $movie
     * @return Response
     * @throws Exception
     */
    public function destroy(Movie $movie)
    {
        //
        $movie->actors()->detach();
        $movie->isPlayed()->detach();
        $movie->delete();
        return response()->json();
    }
}
