@extends('layouts.app')
@section('title', 'Laravel')
@section('content')
    <h1>List Room</h1>
    <table class="table table-striped table-centered">
        <thead>
            <tr>
                <th>{{ __('Name') }}</th>
                <th>{{ __('Capacity') }}</th>
                <th>{{ __('Cinema') }}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($rooms as $room)
            <tr>
                <td>{{ $room->name }}</td>
                <td>{{ $room->capacity }}</td>
                <td>{{ $room->owned->name }}</td>
                <td class="table-action">
                    <a type="button" href="{{ route('room.edit', $room->id) }}" class="btn btn-sm"
                        data-toggle="tooltip" title="@lang('Edit room') {{ $room->name }}">
                        <i class="fas fa-edit fa-lg"></i>
                    </a>
                    <button type="button" onclick="remove(this)" href="{{ route('room.destroy', $room->id) }}" class="btn btn-danger btn-sm"
                        data-toggle="tooltip" title="@lang('Delete room') {{ $room->name }}">
                        <i class="fas fa-trash fa-lg"></i>
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $rooms->appends(request()->except('page'))->links() }}
    <br>
    <a href="room/create" class="create">CREATE</a>
    <script>
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        })
        function remove (e) {
            $.ajax({
                url: $(e).attr('href'),
                type: 'DELETE'
            }).done(function(){
                $(e).closest('tr').remove();
            })
            return false;
        }
    </script>
@endsection
