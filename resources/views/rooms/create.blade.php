@extends('layouts.app')
@section('title', 'Laravel')
@section('content')
    <h1>Create Room</h1>
    <form method="POST" action="{{ route('room.store') }}">
        {{ csrf_field() }}
        <p>
            <label for="name">Name</label>
            <input type="text" name="name" id="name" value="" required />
            <label for="capacity">Capacity</label>
            <input type="number" name="capacity" id="capacity" value="" required />
            <select id="cinema_id" name="cinema_id">
                @foreach($cinemas as $cinema)
                    <option value="{{ $cinema->id }}">{{ $cinema->name }}</option>
                @endforeach
            </select>
        </p>
        <button class="btn btn-success" type="submit">Create</button>
    </form>
@endsection
