@extends('layouts.app')
@section('title', 'Laravel')
@section('content')
    <h1>Edit Room</h1>
    <form method="POST" action="{{ route('room.update', $room->id) }}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <p>
            <label for="name">Name</label>
            <input type="text" name="name" id="name" value="{{ $room->name }}" required />
            <label for="capacity">Capacity</label>
            <input type="number" name="capacity" id="capacity" value="{{ $room->capacity }}" required />
            <select id="cinema_id" name="cinema_id">
                @foreach($cinemas as $cinema)
                    @if($cinema->id == $room->cinema_id)
                        <option selected value="{{ $cinema->id }}">{{ $cinema->name }}</option>
                    @else
                        <option value="{{ $cinema->id }}">{{ $cinema->name }}</option>
                    @endif
                @endforeach
            </select>
        </p>
        <button class="btn btn-warning" type="submit">Update</button>
    </form>
@endsection
