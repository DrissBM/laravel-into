@extends('layouts.app')
@section('title', 'Laravel')
@section('content')
    <h1>List Artist</h1>
    <table class="table table-striped table-centered">
        <thead>
        <tr>
            <th>{{ __('Name') }}</th>
            <th>{{ __('Firstname') }}</th>
            <th>{{ __('Birthdate') }}</th>
            <th>{{ __('Image') }}</th>
            <th>{{ __('Actions') }}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($artists as $artist)
            <tr>
                <td>{{ $artist->name }}</td>
                <td>{{ $artist->firstname }}</td>
                <td>{{ $artist->birthdate }}</td>
                <td><img src="/uploads/face/face_{{ $artist->id }}.png" alt=""></td>
                <td class="table-action">
                    <a type="button" href="{{ route('artist.show', $artist->id) }}" class="btn btn-sm"
                       data-toggle="tooltip" title="@lang('Show artist') {{ $artist->name }}">
                        <i class="far fa-eye"></i>
                    </a>
                    <a type="button" href="{{ route('artist.edit', $artist->id) }}" class="btn btn-sm"
                       data-toggle="tooltip" title="@lang('Edit artist') {{ $artist->name }}">
                        <i class="fas fa-edit fa-lg"></i>
                    </a>
                    <button type="button" onclick="remove(this)" href="{{ route('artist.destroy', $artist->id) }}"
                            class="btn btn-danger btn-sm"
                            data-toggle="tooltip" title="@lang('Delete artist') {{ $artist->name }}">
                        <i class="fas fa-trash fa-lg"></i>
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $artists->appends(request()->except('page'))->links() }}
    <br>
    <a href="artist/create" class="create">CREATE</a>
    <script>
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        })

        function remove(e) {
            $.ajax({
                url: $(e).attr('href'),
                type: 'DELETE'
            }).done(function () {
                $(e).closest('tr').remove();
            })
            return false;
        }
    </script>
@endsection
