@extends('layouts.app')
@section('title', 'Laravel')
@section('content')
    <h1>Edit Artist</h1>
    <form method="POST" action="{{ route('artist.update', $artist->id) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <p>
            <label for="name">Name</label>
            <input type="text" name="name" id="name" value="{{ $artist->name }}" required/>
            <label for="firstname">Firstname</label>
            <input type="text" name="firstname" id="firstname" value="{{ $artist->firstname }}" required/>
            <br>
            <label for="birthdate">Birthdate</label>
            <input type="number" name="birthdate" id="birthdate" value="{{ $artist->birthdate }}" required/>
            <br>
            <label for="img">Add an image:</label>
            <input type="file" id="img" name="img">
        </p>

        <p><img src="/uploads/face/face_{{ $artist->id }}.png" alt=""></p>
        <button class="btn btn-warning" type="submit">Update</button>
    </form>
@endsection
