@extends('layouts.app')
@section('title', 'Laravel')
@section('content')
    {{--    TODO: SHOW ARTIST  afficher sa fiche détaillée  et filmographie--}}
    <h1>Show Artist</h1>
    <p>
        {{ $artist->name }}
        {{ $artist->firstname }}
        {{ $artist->birthdate }}
    </p>

    <p><img src="/uploads/face/face_{{ $artist->id }}.png" alt=""></p>

    <div>
        <h3>Filmographie</h3>
        @foreach ($artist->has_played as $movie)
            <p>
            {{ $movie->title}} <br>
                <img src="/uploads/posters/poster_{{ $movie->id }}.png" alt="">
            </p>
        @endforeach
    </div>
    <br>

    {{-- Boutons delete et edit --}}

    <a type="button" href="{{ route('artist.edit', $artist->id) }}" class="btn btn-sm"
       data-toggle="tooltip" title="@lang('Edit artist') {{ $artist->name }}">
        <i class="fas fa-edit fa-lg"></i>
    </a>
    <button type="button" onclick="remove(this)" href="{{ route('artist.destroy', $artist->id) }}"
            class="btn btn-danger btn-sm"
            data-toggle="tooltip" title="@lang('Delete artist') {{ $artist->name }}">
        <i class="fas fa-trash fa-lg"></i>
    </button>
    <br>
@endsection
