@extends('layouts.app')
@section('title', 'Laravel')
@section('content')
    <h1>Create Artist</h1>
    <form method="POST" action="{{ route('artist.store') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <p>
            <label for="name">Name</label>
            <input type="text" name="name" id="name" value="" required />
            <label for="firstname">Firstname</label>
            <input type="text" name="firstname" id="firstname" value="" required />
            <br>
            <label for="birthdate">Birthdate</label>
            <input type="number" name="birthdate" id="birthdate" value="" required />
            <br>
            <label for="img">Add an image:</label>
            <input type="file" id="img" name="img">
        </p>
        <button class="btn btn-success" type="submit">Create</button>
    </form>
@endsection
