@extends('layouts.app')
@section('title', 'Laravel')
@section('content')
    <div class="flex-center position-ref">
        <div class="content">
            <h2>Movies</h2>
            @foreach($moviesList as $movie)
                {{ $movie->title }} <br>
            @endforeach

            <h2>Actors</h2>
            @foreach($artists as $artist)
                {{ $artist->name }} {{ $artist->firstname }} <br>
            @endforeach

            <h2>Sceance</h2>
            @foreach($seances as $seance)
                <img src="/uploads/posters/poster_{{ $seance->movie_id }}.png" alt=""> <br>
                {{ $seance->title }} <br>
                {{ $seance->name }} <br>
                {{$seance->show_time }} <br>
                <hr>
            @endforeach

        </div>
    </div>
@endsection
