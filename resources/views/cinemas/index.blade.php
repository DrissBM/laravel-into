@extends('layouts.app')
@section('title', 'Laravel')
@section('content')
    <h1>List Cinema</h1>
    <table class="table table-striped table-centered">
        <thead>
            <tr>
                <th>{{ __('Name') }}</th>
                <th>{{ __('Street') }}</th>
                <th>{{ __('NPA') }}</th>
                <th>{{ __('City') }}</th>
                <th>{{ __('Country') }}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($cinemas as $cinema)
            <tr>
                <td>{{ $cinema->name }}</td>
                <td>{{ $cinema->street }}</td>
                <td>{{ $cinema->npa }}</td>
                <td>{{ $cinema->city }}</td>
                <td>{{ $cinema->country }}</td>
                <td class="table-action">
                    <a type="button" href="{{ route('cinema.show', $cinema->id) }}" class="btn btn-sm"
                       data-toggle="tooltip" title="@lang('Show cinema') {{ $cinema->name }}">
                        {{-- TODO: Ajouter un lien sur le nom des cinemas pour le show à la place ou en plus du bouton 'eye' --}}
                        <i class="far fa-eye"></i>
                    </a>
                    <a type="button" href="{{ route('cinema.edit', $cinema->id) }}" class="btn btn-sm"
                        data-toggle="tooltip" title="@lang('Edit cinema') {{ $cinema->name }}">
                        <i class="fas fa-edit fa-lg"></i>
                    </a>
                    <button type="button" onclick="remove(this)" href="{{ route('cinema.destroy', $cinema->id) }}" class="btn btn-danger btn-sm"
                        data-toggle="tooltip" title="@lang('Delete cinema') {{ $cinema->name }}">
                        <i class="fas fa-trash fa-lg"></i>
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $cinemas->appends(request()->except('page'))->links() }}
    <br>
    <a href="cinema/create" class="create">CREATE</a>
    <script>
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        })
        function remove (e) {
            $.ajax({
                url: $(e).attr('href'),
                type: 'DELETE'
            }).done(function(){
                $(e).closest('tr').remove();
            })
            return false;
        }
    </script>
@endsection
