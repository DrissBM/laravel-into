@extends('layouts.app')
@section('title', 'Laravel')
@section('content')
    <h1>Create Cinema</h1>
    <form method="POST" action="{{ route('cinema.store') }}">
        {{ csrf_field() }}
        <p>
            <label for="name">Name</label>
            <input type="text" name="name" id="name" value="" required />
            <br>
            <label for="street">Street</label>
            <input type="text" name="street" id="street" value="" required />
            <label for="npa">NPA</label>
            <input type="text" name="npa" id="npa" value="" required />
            <br>
            <label for="city">City</label>
            <input type="text" name="city" id="city" value="" required />
            <label for="country">Country</label>
            <input type="text" name="country" id="country" value="" required />
        </p>
        <button class="btn btn-success" type="submit">Create</button>
    </form>
@endsection
