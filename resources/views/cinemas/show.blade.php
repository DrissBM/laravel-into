@extends('layouts.app')
@section('title', 'Laravel')
@section('content')
    {{--    TODO: SHOW cinemas  permettant d'afficher la fiche détaillée d'un cinéma (liste des salles, films à l'affiche, liste des séances)--}}
    <h1>Show Cinema</h1>
    <p>{{ $cinema->name }}</p>
    <p>{{ $cinema->street }}</p>
    <p>{{ $cinema->npa }}</p>
    <p>{{ $cinema->city }}</p>
    <p>{{ $cinema->country }}</p>



    @foreach ($cinema->has_owned as $room)
        {{--        <p>--}}
        <h3>{{ $room->name}}</h3>
        <h4>Séances</h4>

        <hr>

        @if($room->play->isEmpty())
            <p>Aucune séance programmée</p>

            <hr>
        @endif

        @foreach($room->play as $seances)
            <img src="/uploads/posters/poster_{{ $seances->id }}.png" alt=""><br>
            - Movie : {{ $seances->title }}<br>
            - Date : {{ $seances->pivot->show_time }}
            <hr>
        @endforeach
    @endforeach


    {{-- Boutons delete et edit --}}

    <a type="button" href="{{ route('cinema.edit', $cinema->id) }}" class="btn btn-sm"
       data-toggle="tooltip" title="@lang('Edit cinema') {{ $cinema->name }}">
        <i class="fas fa-edit fa-lg"></i>
    </a>
    <button type="button" onclick="remove(this)" href="{{ route('cinema.destroy', $cinema->id) }}"
            class="btn btn-danger btn-sm"
            data-toggle="tooltip" title="@lang('Delete cinema') {{ $cinema->name }}">
        <i class="fas fa-trash fa-lg"></i>
    </button>
@endsection
