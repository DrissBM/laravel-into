@extends('layouts.app')
@section('title', 'Laravel')
@section('content')
    <h1>Edit Cinema</h1>
    <form method="POST" action="{{ route('cinema.update', $cinema->id) }}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <p>
            <label for="name">Name</label>
            <input type="text" name="name" id="name" value="{{ $cinema->name }}" required />
            <br>
            <label for="street">Street</label>
            <input type="text" name="street" id="street" value="{{ $cinema->street }}" required />
            <label for="npa">NPA</label>
            <input type="text" name="npa" id="npa" value="{{ $cinema->npa }}" required />
            <br>
            <label for="city">City</label>
            <input type="text" name="city" id="city" value="{{ $cinema->city }}" required />
            <label for="country">Country</label>
            <input type="text" name="country" id="country" value="{{ $cinema->country }}" required />
        </p>
        <button class="btn btn-warning" type="submit">Update</button>
    </form>
@endsection
