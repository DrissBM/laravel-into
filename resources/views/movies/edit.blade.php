@extends('layouts.app')
@section('title', 'Laravel')
@section('content')
    <h1>Edit Movies</h1>
    <form method="POST" action="{{ route('movie.update', $movie->id) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <p>
            <label for="title">Title</label>
            <input type="text" name="title" id="title" value="{{ $movie->title }}" required/>
            <br>
            <label for="year">Year</label>
            <input type="number" name="year" id="year" value="{{ $movie->year }}" required/>
            <br>
            <label for="artist_id">Realisateur</label>
            <select id="artist_id" name="artist_id">
                @foreach($artists as $artist)
                    <option value="{{ $artist->id }}">{{ $artist->name }}</option>
                @endforeach
            </select>
            <label for="actor_id1">Acteur 1</label>
            <select id="actor_id1" name="actor_id1">
                @foreach($artists as $artist)
                    @if($artist->id == $movie->actors->first()->id)
                        <option selected value="{{ $artist->id }}">{{ $artist->name }}</option>
                    @else
                        <option value="{{ $artist->id }}">{{ $artist->name }}</option>
                    @endif
                @endforeach
            </select>
            <label for="actor_id2">Acteur 2</label>
            <select id="actor_id2" name="actor_id2">
                @foreach($artists as $artist)
                    @if($artist->id == $movie->actors->get(1)->id)
                        <option selected value="{{ $artist->id }}">{{ $artist->name }}</option>
                    @else
                        <option value="{{ $artist->id }}">{{ $artist->name }}</option>
                    @endif
                @endforeach
            </select>
            <br>
            <h4>Séance</h4>
            <label for="room_id">Room</label>
            <select id="room_id" name="room_id">
                @foreach($rooms as $room)
                    @if($room->id == $movie->isPlayed->first()->id)
                        <option selected value="{{ $room->id }}">{{ $room->name }}</option>
                    @else
                        <option value="{{ $room->id }}">{{ $room->name }}</option>
                    @endif
                @endforeach
            </select>
            <label for="show_time">Show Time</label>
            <input id="show_time" name="show_time" type="datetime-local" value="{{$date}}"/>
            <br>
            <label for="img">Edit image:</label>
            <input class="d-inline" type="file" id="img" name="img">
            <p>
                <img src="/uploads/posters/poster_{{ $movie->id }}.png" alt="">
            </p>
        </p>
        <button class="btn btn-warning" type="submit">Update</button>
    </form>
@endsection
