@extends('layouts.app')
@section('title', 'Laravel')
@section('content')
    <h1>Show Movie</h1>
    <p>{{ $movie->title }}</p>
    <p>{{ $movie->year }}</p>
    <p>{{ $movie->director->firstname }} {{ $movie->director->name }}</p>
    <p><img src="/uploads/posters/poster_{{ $movie->id }}.png" alt=""></p>

    <h4>Actors :</h4>
    @foreach ($movie->actors as $actor)
        {{ $actor->name }} {{ $actor->firstname }}<br>
    @endforeach

    {{-- Boutons delete et edit --}}
    <br>
    <a type="button" href="{{ route('movie.edit', $movie->id) }}" class="btn btn-sm"
       data-toggle="tooltip" title="@lang('Edit movie') {{ $movie->name }}">
        <i class="fas fa-edit fa-lg"></i>
    </a>
    <button type="button" onclick="remove(this)" href="{{ route('movie.destroy', $movie->id) }}"
            class="btn btn-danger btn-sm"
            data-toggle="tooltip" title="@lang('Delete movie') {{ $movie->title }}">
        <i class="fas fa-trash fa-lg"></i>
    </button>
@endsection
