@extends('layouts.app')
@section('title', 'Laravel')
@section('content')
    <h1>List Movies</h1>
    <table class="table table-striped table-centered">
        <thead>
            <tr>
                <th>{{ __('Title') }}</th>
                <th>{{ __('Year') }}</th>
                <th>{{ __('Real') }}</th>
                <th>{{ __('Image') }}</th>
                <th>{{ __('Actors') }}</th>
                <th>{{ __('Actions') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($movies as $movie)
            <tr>

                <td>{{ $movie->title }}</td>
                <td>{{ $movie->year }}</td>
                <td>{{ $movie->director->firstname }} {{ $movie->director->name }}</td>
                <td><img src="/uploads/posters/poster_{{ $movie->id }}.png" alt=""></td>
                <td>@foreach ($movie->actors as $actor)
                        {{ $actor->name }} {{ $actor->firstname }}<br>
                    @endforeach
                </td>
                <td class="table-action">
                    <a type="button" href="{{ route('movie.show', $movie->id) }}" class="btn btn-sm"
                       data-toggle="tooltip" title="@lang('Show movie') {{ $movie->title }}">
                        {{-- TODO: Ajouter un lien sur le nom des artiste pour le show à la place du bouton 'eye' --}}
                        <i class="far fa-eye"></i>
                    </a>
                    <a type="button" href="{{ route('movie.edit', $movie->id) }}" class="btn btn-sm"
                        data-toggle="tooltip" title="@lang('Edit movie') {{ $movie->title }}">
                        <i class="fas fa-edit fa-lg"></i>
                    </a>
                    <button type="button" onclick="remove(this)" href="{{ route('movie.destroy', $movie->id) }}" class="btn btn-danger btn-sm"
                        data-toggle="tooltip" title="@lang('Delete movie') {{ $movie->title }}">
                        <i class="fas fa-trash fa-lg"></i>
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $movies->appends(request()->except('page'))->links() }}
    <br>
    <a href="movie/create" class="create">CREATE</a>
    <script>
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        })
        function remove (e) {
            $.ajax({
                url: $(e).attr('href'),
                type: 'DELETE'
            }).done(function(){
                $(e).closest('tr').remove();
            })
            return false;
        }
    </script>
@endsection
