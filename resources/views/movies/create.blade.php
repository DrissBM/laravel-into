@extends('layouts.app')
@section('title', 'Laravel')
@section('content')
    <h1>Create Movies</h1>
    <form method="POST" action="{{ route('movie.store') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <p>
            <label for="title">Title</label>
            <input type="text" name="title" id="title" value="" required />
            <br>
            <label for="year">Year</label>
            <input type="number" name="year" id="year" value="" required />
            <br>
            <label for="artist_id">Realisateur</label>
            <select id="artist_id" name="artist_id">
                @foreach($artists as $artist)
                    <option value="{{ $artist->id }}">{{ $artist->name }}</option>
                @endforeach
            </select>
            <label for="actor_id1">Acteur 1</label>
            <select id="actor_id1" name="actor_id1">
                @foreach($artists as $artist)
                    <option value="{{ $artist->id }}">{{ $artist->name }}</option>
                @endforeach
            </select>
            <label for="actor_id2">Acteur 2</label>
            <select id="actor_id2" name="actor_id2">
                @foreach($artists as $artist)
                    <option value="{{ $artist->id }}">{{ $artist->name }}</option>
                @endforeach
            </select>
            <br>
            <h4>Séance</h4>
            <label for="room_id">Room</label>
            <select id="room_id" name="room_id">
                @foreach($rooms as $room)
                    <option value="{{ $room->id }}">{{ $room->name }}</option>
                @endforeach
            </select>
            <label for="show_time">Show Time</label>
            <input id="show_time" name="show_time" type="datetime-local" value="2020-12-20T09:30"/>
            <br>
            <label for="img">Add an image:</label>
            <input type="file" id="img" name="img">
        </p>
        <button class="btn btn-success" type="submit">Create</button>
    </form>
@endsection
