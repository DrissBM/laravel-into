# Laravel Project

This laravel project need PHP v 1.5 minimum

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You need of course node and composer installed on your computer
Clone the project in your /htdocs in your MAMP/WAMP/LAMP ...

### Installing

Firstly, please run the command below for installing all dependencies.

```
npm install
composer install
composer dump-autoload
```

Change .env.example to .env then run 

```
php artisan key:generate
```

Create a database named cinema  
Once the database is created, go into your .env file and change the value DB_DATABASE=laravel into DB_DATABASE=cinema
    
You may also need to change the DB_PORT, DB_USERNAME and DB_PASSWORD values depending on your database.

### Filling the database
In order to create the tables and datas required to start the project, run the following command :  
```
php artisan migrate:fresh --seed
```

### Experience the beauty of this project
Simply access your localhost in your browser, register a new user and log in !


## Authors

* **Vincent Praz**
* **Driss Ben Mimoun**

## License

This project is for crea
